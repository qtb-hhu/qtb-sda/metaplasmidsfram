# boxplot of contig hits in each database (PLSDB, PhageDB, ARSGDB)

datei_df <- read.table("hmm_out/ausgabe_datei_prot.txt", header = T, sep = "\t")
datei_df <- subset(datei_df, select = -1)
colnames(datei_df) <- c("plsDB", "phageDB", "ARSGdb")

boxplot(datei_df,
        ylab = "Number of hits",
        main = "",
        las = 1)
