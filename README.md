# MetaPlasmidsFRAM

## Bioinformatic analysis of plasmid sequences from the Fram Strait

Scripts for reproducing results and figures shown in the bachelor thesis.


## Overview
This thesis presents a descriptive analysis considering statistical and bioinformatic methods, to investigate the presences and genetically information of prokaryotic mobile elements. The underlying meta-genome data was collected as a time series at the mooring station F4 in the Fram Strait.

## Library
Libraries used in this thesis are listed below:
- seqinr
    - D. Charif and J.R. Lobry. SeqinR 1.0-2: a contributed package to the R project for statistical computing devoted to biological sequences retrieval and analysis. 
        In U. Bastolla, M. Porto, H.E. Roman, and M. Vendruscolo, editors, Structural approaches to sequence evolution: Molecules, networks, populations, Biological and
        Medical Physics, Biomedical Engineering, pages 207–232. Springer Verlag, New York,
        2007. ISBN : 978-3-540-35305-8.

- readr
    - Hadley Wickham, Jim Hester, and Jennifer Bryan. readr: Read Rectangular Text Data, 2023. R package version 2.1.4. 

- dplyr
    - Hadley Wickham, Romain François, Lionel Henry, Kirill Müller, and Davis Vaughan. dplyr: A Grammar of Data Manipulation, 2023. R package version 1.1.2.

- ggplot2
    - Hadley Wickham. ggplot2: Elegant Graphics for Data Analysis. Springer-Verlag New York, 2016.

- ggOceanMaps
    - Mikko Vihtakari. ggOceanMaps: Plot Data on Oceanographic Maps using ’ggplot2’, 2024. R package version 2.2.0.

- reshape2
    - Hadley Wickham. Reshaping data with the reshape package. Journal of Statistical Software, 21(12):1–20, 2007.

- data.table
    - Matt Dowle and Arun Srinivasan. data.table: Extension of ‘data.frame‘, 2023. R package version 1.14.8.2

    

## Contributors

Christian Dohmann

## Acknowledgement

Special thanks to Dr. Ovidiu Popa for supervising and supporting this work.

## License

MIT license






